# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the akonadi-calendar package.
#
# SPDX-FileCopyrightText: 2022, 2024, 2025 Emir SARI <emir_sari@icloud.com>
msgid ""
msgstr ""
"Project-Id-Version: akonadi-calendar\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-09-19 00:40+0000\n"
"PO-Revision-Date: 2025-02-21 09:54+0300\n"
"Last-Translator: Emir SARI <emir_sari@icloud.com>\n"
"Language-Team: Turkish <kde-l10n-tr@kde.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 25.03.70\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Emir SARI"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "emir_sari@icloud.com"

#: alarmnotification.cpp:50
#, kde-format
msgid "View"
msgstr "Görünüm"

#: alarmnotification.cpp:57
#, kde-format
msgid "Task"
msgstr "Görev"

#: alarmnotification.cpp:57
#, kde-format
msgid "Event"
msgstr "Etkinlik"

#: alarmnotification.cpp:60
#, kde-format
msgid "Task due at %1"
msgstr "Görevin bitiş tarihi %1"

#: alarmnotification.cpp:64
#, kde-format
msgctxt "Event starts in 5 minutes"
msgid "%2 starts in %1 minute"
msgid_plural "%2 starts in %1 minutes"
msgstr[0] "%2, %1 dakika içinde başlıyor"
msgstr[1] "%2, %1 dakika içinde başlıyor"

#: alarmnotification.cpp:68
#, kde-format
msgctxt "Event starts at 10:00"
msgid "%1 starts at %2"
msgstr "%1, %2 saatinde başlıyor"

#: alarmnotification.cpp:70
#, kde-format
msgctxt "Event started at 10:00"
msgid "%1 started at %2"
msgstr "%1, %2 saatinde başladı"

#: alarmnotification.cpp:76
#, kde-format
msgctxt "Event starts on <DATE> at <TIME>"
msgid "%1 starts on %2 at %3"
msgstr "%1, %2 tarihinde %3 saatinde başlıyor"

#: alarmnotification.cpp:82
#, kde-format
msgctxt "Event started on <DATE> at <TIME>"
msgid "%1 started on %2 at %3"
msgstr "%1; %2 tarihinde %3 saatinde başladı"

#: alarmnotification.cpp:91
#, kde-format
msgctxt "Event starts on <DATE>"
msgid "%1 starts on %2"
msgstr "%1, %2 tarihinde başlıyor"

#: alarmnotification.cpp:93
#, kde-format
msgctxt "Event started on <DATE>"
msgid "%1 started on %2"
msgstr "%1, %2 tarihinde başladı"

#: alarmnotification.cpp:118
#, kde-format
msgid "Remind in 5 mins"
msgstr "5 Dakika Sonra Anımsat"

#: alarmnotification.cpp:124
#, kde-format
msgid "Remind later..."
msgstr "Daha Sonra Anımsat…"

#: alarmnotification.cpp:130
#, kde-format
msgctxt "dismiss a reminder notification for an event"
msgid "Dismiss"
msgstr "Geç"

#: alarmnotification.cpp:206
#, kde-format
msgid "Open URL"
msgstr "URL’yi Aç"

#: alarmnotification.cpp:224
#, kde-format
msgid "Map"
msgstr "Harita"

#: kalendaracmain.cpp:25
#, kde-format
msgctxt "@title"
msgid "Reminders"
msgstr "Anımsatıcılar"

#: kalendaracmain.cpp:28
#, kde-format
msgid "Calendar Reminder Service"
msgstr "Takvim Anımsatıcısı Hizmeti"

#: kalendaracmain.cpp:32
#, kde-format
msgid "(c) KDE Community 2021-2024"
msgstr "© KDE Topluluğu 2021–2024"

#: kalendaracmain.cpp:33
#, kde-format
msgctxt "@info:credit"
msgid "Carl Schwan"
msgstr "Carl Schwan"

#: kalendaracmain.cpp:34 kalendaracmain.cpp:38
#, kde-format
msgctxt "@info:credit"
msgid "Maintainer"
msgstr "Bakımcı"

#: kalendaracmain.cpp:37
#, kde-format
msgctxt "@info:credit"
msgid "Clau Cambra"
msgstr "Clau Cambra"

#: suspenddialog.cpp:74
#, kde-format
msgctxt "@label:spinbox"
msgid "Suspend &duration:"
msgstr "Askıya alma &süresi:"

#: suspenddialog.cpp:81
#, kde-format
msgctxt "@info:tooltip"
msgid "Suspend the reminders by this amount of time"
msgstr "Anımsatıcıları bu süre kadar askıya al"

#: suspenddialog.cpp:83
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"Each reminder for the selected incidences will be suspended by this number "
"of time units. You can choose the time units (typically minutes) in the "
"adjacent selector."
msgstr ""
"Seçili olaylara ilişkin her anımsatıcı bu sayıda zaman birimi kadar askıya "
"alınır. Bitişikteki seçiciden zaman birimlerini (genelde dakika) "
"seçebilirsiniz."

#: suspenddialog.cpp:91
#, kde-format
msgctxt "@item:inlistbox suspend in terms of minutes"
msgid "minute(s)"
msgstr "dakika"

#: suspenddialog.cpp:92
#, kde-format
msgctxt "@item:inlistbox suspend in terms of hours"
msgid "hour(s)"
msgstr "saat"

#: suspenddialog.cpp:93
#, kde-format
msgctxt "@item:inlistbox suspend in terms of days"
msgid "day(s)"
msgstr "gün"

#: suspenddialog.cpp:94
#, kde-format
msgctxt "@item:inlistbox suspend in terms of weeks"
msgid "week(s)"
msgstr "hafta"

#: suspenddialog.cpp:95
#, kde-format
msgctxt "@info:tooltip"
msgid "Suspend the reminders using this time unit"
msgstr "Anımsatıcıları bu zaman birimini kullanarak askıya al"

#: suspenddialog.cpp:97
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"Each reminder for the selected incidences will be suspended using this time "
"unit. You can set the number of time units in the adjacent number entry "
"input."
msgstr ""
"Seçili olaylara ilişkin her anımsatıcı bu zaman birimi kullanılarak askıya "
"alınır. Bitişikteki seçiciden zaman birimlerini (genelde dakika) "
"seçebilirsiniz."

#: suspenddialog.cpp:110
#, kde-format
msgctxt "@action:button"
msgid "Remind Later"
msgstr "Daha Sonra Anımsat"

#: suspenddialog.cpp:111
#, kde-format
msgctxt "@info:tooltip"
msgid "Remind me again after the specified interval"
msgstr "Belirtilen aralıktan sonra yeniden anımsat"

#: suspenddialog.cpp:112
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"Press this button to be reminded again about this incidence after the "
"specified amount of time."
msgstr ""
"Belirtilen süre sonunda bu olayın yeniden anımsatılması için bu düğmeye "
"basın."

#: suspenddialog.cpp:116
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr "İptal"

#: suspenddialog.cpp:117
#, kde-format
msgctxt "@info:tooltip"
msgid "Cancel reminding later, show the notification again"
msgstr "Daha sonra anımsatmayı iptal et, bildirimi yeniden göster"

#~ msgctxt "@info:whatsthis"
#~ msgid "Press this button to silence the reminder forever."
#~ msgstr "Anımsatıcıyı sonsuza kadar susturmak için bu düğmeye basın."
